package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        return findNumberRecursively(number, 0, 10000); // assuming the maximum possible number is 10,000
    }

    private int findNumberRecursively(RandomNumber number, int low, int high) {
        if (low > high) {
            return -1; // this should never happen if everything is working correctly
        }

        int mid = (low + high) / 2;
        int guessResult = number.guess(mid);

        if (guessResult == 0) {
            return mid;
        } else if (guessResult == -1) {
            return findNumberRecursively(number, mid + 1, high);
        } else {
            return findNumberRecursively(number, low, mid - 1);
        }
    }
}
