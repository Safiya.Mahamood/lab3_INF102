package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    // recursive binary search algorithm
    // comparing the mid of the search space with the key, and based on the result either return the index where the key is found
    // or call the recursive function for the next search space
    // low ->    mid = low + (high - low) / 2      <- high


    public static int findPeak(List<Integer> arr, int low, int high){
        if (high >= low) {
            int mid = low + (high - low) / 2;

            if ((mid == 0 || arr.get(mid - 1) <= arr.get(mid)) && (mid == arr.size() - 1 || arr.get(mid + 1) <= arr.get(mid))) {
                return mid;
            }

            if (mid - 1 >= 0 && arr.get(mid - 1) > arr.get(mid)){
                return findPeak(arr, low, mid - 1);
            }

            else {
                return findPeak(arr, mid + 1, high);
            }
        }

        return -1;
    }


    @Override
    public int peakElement(List<Integer> numbers) {
        int peak = findPeak(numbers, 0, numbers.size() - 1);

        return numbers.get(peak);
    }

}
