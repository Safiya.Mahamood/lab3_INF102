package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if (list.isEmpty()){
            return 0;
        }
        else {
            long sum = list.get(0);
            List<Long> restOfList = list.subList(1, list.size());
            return sum + sum(restOfList); // 0 + [1, 2...] til listen er tom
        }
    }
    

}
